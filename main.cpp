#include <Core.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QDownloadManager *Manager = new QDownloadManager();
    for(int i = 0; i < 20; i++)
        Manager->addDownload("DL" + QString::number(i), "VAT3");
    QDownloadManagerWidget w(Manager);
    w.show();

    return a.exec();
}
