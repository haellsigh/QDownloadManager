#ifndef QDOWNLOADMANAGER_H
#define QDOWNLOADMANAGER_H

#include <Core.h>

#include <QtNetwork>
#include <QTimer>
#include <QSignalMapper>
#include <QMap>
#include <QNetworkReply>

class QDownloadManager : public QObject
{
    Q_OBJECT
public:
    explicit QDownloadManager(QObject *parent = 0);
    ~QDownloadManager();

    void addDownload(const QString name, const QUrl url,    const int delay = 0);
    void addDownload(const QString name, const QString url, const int delay = 0)
    {
        addDownload(name, QUrl(url), delay);
    }

    QByteArray retrieveDownloadedData(QString name);

    QList<Download> getCurrentDownloads();

signals:
    void downloadFinished(QString name);

public slots:
    void slotDownloadFinished(QString name);
    bool dataAvailable(QString name);
    bool isRunning(QString name);

private:
    QNetworkAccessManager *mNetworkManager;
    QNetworkDiskCache *mDiskCache;
    QSignalMapper *mSignalMapper;
    QMap<QString, QNetworkReply*> mDownloadReplies;

};

#endif // QDOWNLOADMANAGER_H
