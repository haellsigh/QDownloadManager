#include "QDownloadManager.h"

//Network disk cache maximum size in Mb
#define CACHE_SIZE 150


QDownloadManager::QDownloadManager(QObject *parent) : QObject(parent)
{
    mSignalMapper = new QSignalMapper(this);
    mSignalMapper->setParent(this);
    mNetworkManager = new QNetworkAccessManager(this);
    mDiskCache = new QNetworkDiskCache(this);
    mDiskCache->setCacheDirectory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    mDiskCache->setMaximumCacheSize(CACHE_SIZE * 1024 * 1024);
    mDiskCache->setParent(mNetworkManager);
    mNetworkManager->setCache(mDiskCache);
    mNetworkManager->setParent(this);

    connect(mSignalMapper, SIGNAL(mapped(QString)), SLOT(slotDownloadFinished(QString)));
}

QDownloadManager::~QDownloadManager()
{
    foreach (QNetworkReply *reply, mDownloadReplies) {
        delete reply;
    }
}

void QDownloadManager::addDownload(const QString name, const QUrl url, const int delay)
{
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
    mDownloadReplies[name] = mNetworkManager->get(request);
    qDebug() << "Cache size: " << mNetworkManager->cache()->cacheSize();
    mDownloadReplies[name]->setParent(this);
    connect(mDownloadReplies[name], SIGNAL(finished()), mSignalMapper, SLOT(map()));
    mSignalMapper->setMapping(mDownloadReplies[name], name);
}

QByteArray QDownloadManager::retrieveDownloadedData(QString name)
{
    if(!dataAvailable(name))
        return QByteArray("ERROR");
    QByteArray data = mDownloadReplies[name]->readAll();
    mDownloadReplies[name]->deleteLater();
    mDownloadReplies.remove(name);
    return data;
}

QList<Download> QDownloadManager::getCurrentDownloads()
{
    QMapIterator<QString, QNetworkReply*> i(mDownloadReplies);
    QList<Download> Downloads;

    while(i.hasNext()) {
        i.next();

        Downloads.append(Download(i.key(), 0, 0));
    }
    return Downloads;
}

void QDownloadManager::slotDownloadFinished(QString name)
{
    bool toRedirect = false;
    QUrl possibleRedirectUrl = mDownloadReplies[name]->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    if(!possibleRedirectUrl.isEmpty())
        toRedirect = true;

    if(toRedirect)
    {
        mDownloadReplies[name]->deleteLater();
        mDownloadReplies.remove(name);
        addDownload(name, possibleRedirectUrl);
    }
    else
        emit downloadFinished(name);
}

bool QDownloadManager::dataAvailable(QString name)
{
    if(mDownloadReplies.contains(name))
        return mDownloadReplies[name]->isFinished();
    return false;
}

bool QDownloadManager::isRunning(QString name)
{
    if(mDownloadReplies.contains(name))
        return mDownloadReplies[name]->isRunning();
    return false;
}
