#-------------------------------------------------
#
# Project created by QtCreator 2015-03-25T22:11:36
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    QDownloadManager
#TEMPLATE =  app

include(QDownloadManager.pri)

SOURCES += \
    main.cpp
