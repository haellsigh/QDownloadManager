INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/QDownloadManager.cpp \
    $$PWD/QDownloadManagerWidget.cpp
HEADERS += \
    $$PWD/QDownloadManager.h \
    $$PWD/Core.h \
    $$PWD/QDownloadManagerWidget.h

FORMS += \
    $$PWD/QDownloadManagerWidget.ui
