#include "QDownloadManagerWidget.h"
#include "ui_QDownloadManagerWidget.h"

QDownloadManagerWidget::QDownloadManagerWidget(QDownloadManager* downloadManager) :
    ui(new Ui::QDownloadManagerWidget)
{
    ui->setupUi(this);
    mDownloadManager = downloadManager;

    mDownloadLayout = new QGridLayout;
    mDownloadLayout->setColumnStretch(0, 1);
    mDownloadLayout->setColumnStretch(1, 0);
    mDownloadLayout->setHorizontalSpacing(0);
    ui->scrollAreaWidgetContents->setLayout(mDownloadLayout);

    int i = 0;
    foreach (Download curDl, mDownloadManager->getCurrentDownloads()) {
        mDownloadLayout->addWidget(new QProgressBar, i, 0);
        mDownloadLayout->addWidget(new QLabel(curDl.mName), i, 1);
        i++;
    }
}

QDownloadManagerWidget::~QDownloadManagerWidget()
{
    delete mDownloadLayout;
    delete ui;
}
