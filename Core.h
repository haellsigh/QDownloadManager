#ifndef CORE
#define CORE

#include <QString>

struct Download
{
    qint64 mDownloaded;
    qint64 mTotal;
    QString mName;

    Download(QString name, qint64 total, qint64 downloaded) : mDownloaded(downloaded), mTotal(total), mName(name)
    {}
};
typedef struct Download Download;

#include "QDownloadManager.h"
#include "QDownloadManagerWidget.h"

#endif // CORE

