QDownloadManager documentation
============================================

Welcome to the `QDownloadManager` documentation.

For any questions, feature requests or issues with the code or the documentation please use our `GitHub Issue tracker`.
