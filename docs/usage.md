# Usage

### Constructor

##### QDownloadManager::QDownloadManager(QObject *parent)

Example use inside your main window:

    QDownloadManager *mDownloadManager = new QDownloadManager(this);

---
### Adding a download

##### QDownloadManager::addDownload(const QString name, const QUrl url, const int delay)

Add a download with name "name", downloading ressource at url "url".
The download automatically starts after "delay" milliseconds.
This is useful in case you want to cancel the download before starting it. (See [QDownloadManager::removeDownload()]() )

Example use (assuming mDownloadManager is a pointer to a valid instance):

    mDownloadManager.addDownload("MainWindowBackground", "https://placekitten.com/g/200/150", 100);

---
