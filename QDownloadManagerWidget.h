#ifndef QDOWNLOADMANAGERWIDGET_H
#define QDOWNLOADMANAGERWIDGET_H

#include <Core.h>

#include <QWidget>
#include <QtWidgets>

class QDownloadManager;

namespace Ui {
class QDownloadManagerWidget;
}

class QDownloadManagerWidget : public QWidget
{
    Q_OBJECT

public:
    QDownloadManagerWidget(QDownloadManager *downloadManager);
    ~QDownloadManagerWidget();

private:
    Ui::QDownloadManagerWidget *ui;
    QGridLayout *mDownloadLayout;

    QDownloadManager *mDownloadManager;
};

#endif // QDOWNLOADMANAGERWIDGET_H
